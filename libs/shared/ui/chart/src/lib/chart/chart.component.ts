import {
  Component,
  Input
} from '@angular/core';
import { Chart, ChartData } from './chart.type';
import { CHART } from './chart';

@Component({
  selector: 'coding-challenge-chart',
  templateUrl: './chart.component.html'
})
export class ChartComponent {
  @Input() chartData: ChartData;
  chart: Chart = CHART;
}
