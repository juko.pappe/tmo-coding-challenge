import { Chart } from './chart.type';

export const CHART: Chart = {
  title: '',
  type: 'LineChart',
  data: [],
  columnNames: ['period', 'close'],
  options: { title: `Stock price`, width: '600', height: '400' }
};
