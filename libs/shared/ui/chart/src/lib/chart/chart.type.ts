export type Chart = {
  title: string;
  type: string;
  data: ChartData;
  columnNames: string[];
  options: ChartOptions;
};

export type ChartData = (string | number)[][];

export type ChartOptions = {
  title: string;
  width: string;
  height: string;
};
