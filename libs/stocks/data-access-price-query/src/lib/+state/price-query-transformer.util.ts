import { PriceQueryResponse, PriceQuery } from './price-query.type';
import { map, pick } from 'lodash-es';
import { parse } from 'date-fns';
import { HttpErrorResponse } from '@angular/common/http';

export function transformPriceQueryResponse(
  response: PriceQueryResponse[]
): PriceQuery[] {
  return map(
    response,
    responseItem =>
      ({
        ...pick(responseItem, [
          'date',
          'open',
          'high',
          'low',
          'close',
          'volume',
          'change',
          'changePercent',
          'label',
          'changeOverTime'
        ]),
        dateNumeric: parse(responseItem.date).getTime()
      } as PriceQuery)
  );
}

export function filterPriceQueryByDate(
  list: PriceQuery[], startDate: Date, endDate: Date): PriceQuery[] {
  return list.filter(item =>
    endDate >= convertToDate(item.date) && convertToDate(item.date) >= startDate);
}

function convertToDate(date: string): Date {
  return new Date(date);
}

export function httpErrorHandler(httpError: HttpErrorResponse): string {
  if (httpError && httpError.error && httpError.error.statusCode) {
    return httpError.error.error;
  }
  return httpError && httpError.error ?
    httpError.error : 'A general error has occurred on request.';
}
