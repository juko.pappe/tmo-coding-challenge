import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StocksComponent } from './stocks/stocks.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: StocksComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StocksFeatureShellRoutingModule { }
