import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StocksComponent } from './stocks.component';
import { ReactiveFormsModule } from '@angular/forms';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { PriceQueryFacade } from '@coding-challenge/stocks/data-access-price-query';
import { Observable } from 'rxjs';
import {
  MatDatepickerModule,
  MatFormFieldModule,
  MatInputModule,
  MatNativeDateModule,
  MatSelectModule
} from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TimePeriodValues } from './stocks.utils';

describe('StocksComponent', () => {
  let component: StocksComponent;
  let fixture: ComponentFixture<StocksComponent>;
  let priceQueryFacade;
  let SYMBOL, PERIOD, START_DATE, END_DATE;

  beforeEach(async(() => {
    priceQueryFacade = {
      priceQueries$: new Observable((observer) => {
        observer.next([['2017', 320]]);
      }),
      fetchQuote: (symbol, period, startDate, endDate) => {
        SYMBOL = symbol;
        PERIOD = period;
        START_DATE = startDate;
        END_DATE = endDate;
      }
    };
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        MatInputModule,
        MatSelectModule,
        MatFormFieldModule,
        BrowserAnimationsModule,
        MatDatepickerModule,
        MatNativeDateModule
      ],
      providers: [
        { provide: PriceQueryFacade, useValue: priceQueryFacade }
      ],
      declarations: [ StocksComponent ],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StocksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('test ngOnInit', () => {
    component.quotes$ = null;
    component.stockPickerForm = null;

    expect(component.stockPickerForm).toBeNull();
    expect(component.quotes$).toBeNull();

    component.ngOnInit();

    expect(component.quotes$).toBeDefined();
    expect(component.stockPickerForm.controls.symbol).toBeDefined();
    expect(component.stockPickerForm.controls.period).toBeDefined();
    expect(component.stockPickerForm.controls.startDate).toBeDefined();
    expect(component.stockPickerForm.controls.endDate).toBeDefined();
  });

  describe('test form', () => {
    beforeEach(() => {
      component.stockPickerForm.reset();
    });

    it('test form with valid values', () => {
      fillFormWithValidValues();
      expect(component.stockPickerForm.valid).toBeTruthy();
    });

    it('test form with invalid values', () => {
      expect(component.stockPickerForm.valid).toBeFalsy();
    });

    it('test form field symbol with invalid values', () => {
      expect(component.stockPickerForm.controls.symbol.valid).toBeFalsy();
    });

    it('test form field period with invalid values', () => {
      expect(component.stockPickerForm.controls.period.valid).toBeFalsy();
    });
  });

  describe('tests for function startDateChanged', () => {
    let date: Date;

    beforeEach(() => {
      date = new Date();
      component.stockPickerForm.controls.startDate.setValue(date);
      component.stockPickerForm.controls.endDate.setValue(null);
      spyOn(component, 'setTimePeriod');
      spyOn(component, 'fetchQuote');
    });

    it('test startDateChanged when startDate set', () => {
      component.startDateChanged();

      const endDate: Date = component.stockPickerForm.controls.endDate.value;

      expect(endDate.getFullYear()).toBe(date.getFullYear());
      expect(endDate.getMonth()).toBe(date.getMonth());
      expect(endDate.getDay()).toBe(date.getDay());
      expect(endDate).toBe(component.maxDate);
      expect(component.setTimePeriod).toHaveBeenCalled();
      expect(component.fetchQuote).toHaveBeenCalled();
    });

    it('test startDateChanged when startDate is not set', () => {
      component.stockPickerForm.controls.startDate.setValue(null);
      component.startDateChanged();

      expect(component.stockPickerForm.controls.endDate.value)
        .toBe(null);
      expect(component.setTimePeriod).not.toHaveBeenCalled();
      expect(component.fetchQuote).not.toHaveBeenCalled();
    });
  });

  it('test function setTimePeriod', () => {
    component.stockPickerForm.controls.startDate.setValue(new Date());
    component.setTimePeriod();
    expect(component.stockPickerForm.controls.period.value)
      .toBe(TimePeriodValues.oneMonth);
  });

  describe('test function fetchQuote', () => {
    it('test fetchQuote if form valid & endDate defined', () => {
      fillFormWithValidValues();
      component.fetchQuote();
      expect(SYMBOL).toBe('AAPL');
      expect(PERIOD).toBe(TimePeriodValues.oneMonth);
      expect(START_DATE).toBe(END_DATE);
      expect(START_DATE.getFullYear()).toBe(new Date().getFullYear());
    });

    it('test fetchQuote if form valid & endDate not defined', () => {
      fillFormWithValidValues();
      component.stockPickerForm.controls.endDate.setValue(null);
      component.fetchQuote();
      expect(SYMBOL).toBe('AAPL');
      expect(PERIOD).toBe(TimePeriodValues.oneMonth);
      expect(START_DATE.getFullYear()).toBe(new Date().getFullYear());
      expect(END_DATE).toBe(component.maxDate);
    });

    it('test fetchQuote else if path', () => {
      component.stockPickerForm.reset();

      component.fetchQuote(false);
      expect(component.stockPickerForm.controls.symbol.touched).toBeTruthy();
      expect(component.stockPickerForm.controls.period.touched).toBeTruthy();
      expect(component.stockPickerForm.controls.startDate.touched).toBeTruthy();
      expect(component.stockPickerForm.controls.endDate.touched).toBeTruthy();
    });
  });

  function fillFormWithValidValues() {
    const date = new Date();
    const formInputs = component.stockPickerForm.controls;
    formInputs['symbol'].setValue('AAPL');
    formInputs['period'].setValue(TimePeriodValues.oneMonth);
    formInputs['startDate'].setValue(date);
    formInputs['endDate'].setValue(date);
  }
});
