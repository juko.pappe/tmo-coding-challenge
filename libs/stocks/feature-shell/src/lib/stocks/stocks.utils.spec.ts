import { getTimePeriodFromDate, TimePeriodValues } from './stocks.utils';

describe('StocksUtils', () => {
  const DATE: Date = new Date();
  const CURRENT_YEAR: number = DATE.getFullYear();
  const CURRENT_MONTH: number = DATE.getMonth();
  const CURRENT_DAY: number = DATE.getDay();

  it('test getTimePeriodFromDate for oneMonth', () => {
    const result = getTimePeriodFromDate(new Date());
    expect(result).toBe(TimePeriodValues.oneMonth);
  });

  it('test getTimePeriodFromDate for threeMonths', () => {
    const result = getTimePeriodFromDate(new Date(CURRENT_YEAR, CURRENT_MONTH - 2, CURRENT_DAY));
    expect(result).toBe(TimePeriodValues.threeMonths);
  });

  it('test getTimePeriodFromDate for sixMonths', () => {
    const result = getTimePeriodFromDate(new Date(CURRENT_YEAR, CURRENT_MONTH - 5, CURRENT_DAY));
    expect(result).toBe(TimePeriodValues.sixMonths);
  });

  it('test getTimePeriodFromDate for oneYear', () => {
    const result = getTimePeriodFromDate(new Date(CURRENT_YEAR, CURRENT_MONTH - 9, CURRENT_DAY));
    expect(result).toBe(TimePeriodValues.oneYear);
  });

  it('test getTimePeriodFromDate for twoYears', () => {
    const result = getTimePeriodFromDate(new Date(CURRENT_YEAR - 1, CURRENT_MONTH, CURRENT_DAY));
    expect(result).toBe(TimePeriodValues.twoYears);
  });

  it('test getTimePeriodFromDate for fiveYears', () => {
    const result = getTimePeriodFromDate(new Date(CURRENT_YEAR - 4, CURRENT_MONTH, CURRENT_DAY));
    expect(result).toBe(TimePeriodValues.fiveYears);
  });
});
