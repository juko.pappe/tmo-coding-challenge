import { TimePeriod } from './stocks.type';

const DATE: Date = new Date();
const CURRENT_YEAR: number = DATE.getFullYear();
const CURRENT_MONTH: number = DATE.getMonth();
const CURRENT_DAY: number = DATE.getDay();

export enum TimePeriodValues {
  max = 'max',
  fiveYears = '5y',
  twoYears = '2y',
  oneYear = '1y',
  yearToDate = 'ytd',
  sixMonths = '6m',
  threeMonths = '3m',
  oneMonth = '1m'
}
export const TIME_PERIODS: TimePeriod[] = [
  { viewValue: 'All available data', value: TimePeriodValues.max },
  { viewValue: 'Five years', value: TimePeriodValues.fiveYears },
  { viewValue: 'Two years', value: TimePeriodValues.twoYears },
  { viewValue: 'One year', value: TimePeriodValues.oneYear },
  { viewValue: 'Year-to-date', value: TimePeriodValues.yearToDate },
  { viewValue: 'Six months', value: TimePeriodValues.sixMonths },
  { viewValue: 'Three months', value: TimePeriodValues.threeMonths },
  { viewValue: 'One month', value: TimePeriodValues.oneMonth }
];

export function getTimePeriodFromDate(date: Date): string {
  switch(true) {
    case date > getMonthsAgoDate(1):
      return TimePeriodValues.oneMonth;
    case date > getMonthsAgoDate(3):
      return TimePeriodValues.threeMonths;
    case date > getMonthsAgoDate(6):
      return TimePeriodValues.sixMonths;
    case date > new Date(CURRENT_YEAR, 0, 1):
      return TimePeriodValues.yearToDate;
    case date > getYearsAgoDate(1):
      return TimePeriodValues.oneYear;
    case date > getYearsAgoDate(2):
      return TimePeriodValues.twoYears;
    case date > getYearsAgoDate(5):
      return TimePeriodValues.fiveYears;
    default:
      return TimePeriodValues.max;
  }
}

function getMonthsAgoDate(months: number): Date {
  return new Date(CURRENT_YEAR, CURRENT_MONTH - months, CURRENT_DAY);
}

function getYearsAgoDate(year: number): Date {
  return new Date(CURRENT_YEAR - year, CURRENT_MONTH, CURRENT_DAY);
}
