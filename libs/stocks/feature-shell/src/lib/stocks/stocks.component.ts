import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PriceQueryFacade } from '@coding-challenge/stocks/data-access-price-query';
import { Observable } from 'rxjs';
import { ChartData } from '@coding-challenge/shared/ui/chart';
import { TIME_PERIODS, getTimePeriodFromDate } from './stocks.utils';
import { TimePeriod } from './stocks.type';

@Component({
  selector: 'coding-challenge-stocks',
  templateUrl: './stocks.component.html',
  styleUrls: ['./stocks.component.css']
})
export class StocksComponent implements OnInit {
  stockPickerForm: FormGroup;
  symbol: string;
  period: string;
  timePeriods: TimePeriod[] = TIME_PERIODS;

  quotes$: Observable<ChartData>;
  error$: Observable<string>;

  maxDate: Date = new Date();

  constructor(private fb: FormBuilder, private priceQuery: PriceQueryFacade) {}

  ngOnInit() {
    this.quotes$ = this.priceQuery.priceQueries$;
    this.error$ = this.priceQuery.error$;
    this.createForm();
  }

  private createForm(): void {
    this.stockPickerForm = this.fb.group({
      symbol: [null, Validators.required],
      period: [null, Validators.required],
      startDate: [null],
      endDate: [null]
    });
  }

  startDateChanged(): void {
    const startDate = this.stockPickerForm.get('startDate');
    if (startDate.value && startDate.valid) {
      this.stockPickerForm.get('endDate').setValue(this.maxDate);
      this.setTimePeriod();
      this.fetchQuote();
    } else {
      this.stockPickerForm.get('endDate').setValue(null);
    }
  }

  setTimePeriod(): void {
    this.stockPickerForm.get('period').setValue(
      getTimePeriodFromDate(this.stockPickerForm.get('startDate').value)
    );
  }

  fetchQuote(ignoreErrors: boolean = true): void {
    if (this.stockPickerForm.valid) {
      const { symbol, period, startDate } = this.stockPickerForm.value;
      let endDate: Date = this.stockPickerForm.get('endDate').value;
      if (!endDate) {
        endDate = this.maxDate;
      }
      this.priceQuery.fetchQuote(symbol, period, startDate, endDate);
    } else if (!ignoreErrors) {
      this.markFormTouched();
    }
  }

  private markFormTouched(): void {
    Object.keys(this.stockPickerForm.controls).map(key => {
      this.stockPickerForm.controls[key].markAsTouched();
    });
  }
}
