# Code review

## Done well

In a other projects I have used npm link to connect libraries to apps.
Using npm link (with all the setup) is a bit tiresome so using a monorepo
seems a lot more developer friendly.

Also monorepo ensures consistent versioning of dependencies through the project.

Good that libraries have been created by the typical Nx types
* feature
* ui
* data-access
* util

It is good that library paths are defined in main tsconfig.json files
```
// THIS WOULD BE BAD AS PATHS CAN CHANGE
import { PriceQueryFacade } from '../../../../data-access-price-query/src/index';

// THIS IS GOOD
import { PriceQueryFacade } from '@coding-challenge/stocks/data-access-price-query';
```
Also it is **good that linting rules are defined** so if someone tries
to do something wrong (like the example wrong import) it will be found.

Good that lazy loading is used. That increases apps performance.

I like that there are README files inside every library with technical information

## Would change/add/remove

I would add tests for discovering and avoiding bugs

I would remove the empty style files, they can be added later if there is a need.

I would remove not used variables and imports

I also think that using scss or sass instead of css would be better
(they add more ways to organize styles https://www.toptal.com/front-end/embracing-sass-why-you-should-stop-using-vanilla-css).

I would use clear type for variables as much as possible.
For example in lib ui in charts.component.ts would remove any
```
 chartData: any;

  chart: {
    title: string;
    type: string;
    data: any;
    columnNames: string[];
    options: any;
  };
```

I would use exported const values for data that is not changed in components
like the chart in charts.component.ts.
```
  this.chart = {
    title: '',
    type: 'LineChart',
    data: [],
    columnNames: ['period', 'close'],
    options: { title: `Stock price`, width: '600', height: '400' }
  };
```

Like:
* timePeriods in feature-shell stock.component.ts
* chart object in ui chart.component.ts

I would have a separate file for Routing and import that into the app.module.ts
```
  // would move this into a seperate file
  RouterModule.forRoot([
    {
      path: '',
      loadChildren:
        '@coding-challenge/stocks/feature-shell#StocksFeatureShellModule'
    }
  ]),
```

In stocks.component.ts I would make a separate function for form initialization
and call that from ngOnInit not from constructor.

## Problematic points

In stocks.component.html there is a html formatting error I believe
```
  <mat-error
      ><span
        *ngIf="
          !stockPickerForm.get('symbol').valid &&
          stockPickerForm.get('symbol').touched
        "
        >Please enter a symbol</span
      >
  </mat-error>
  // SHOULD BE SOMETHING LIKE
  <mat-error>
      <span
        *ngIf="
            !stockPickerForm.get('symbol').valid &&
            stockPickerForm.get('symbol').touched">
        Please enter a symbol
      </span>
  </mat-error>
```

Some of the tests are broken

In price-query.facade.ts skip(1) is used on the observable, that causes some
of the data to be ignored.

In chart component there are few problems
* In chart element in html ngIf uses an undefined variable _data_
  should use _chartData_
* @Input() data$: Observable<any>; I would not pass an observable
  here as an input. I would pass only the prepared data.
  * Also at the moment there is a memory leak as at the moment we do not
  unsubscribe from the this.data$.subscribe(newData => (this.chartData = newData));
  Ways to fix memory leaks ()



