/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 **/
import { Server } from 'hapi';
import { HttpErrorResponse } from '@angular/common/http';

const init = async () => {
  const BASE_PATH = 'https://sandbox.iexapis.com';
  const ONE_MINUTE = 60000;
  const server: Server = new Server({
    port: 3333,
    host: 'localhost',
    routes: {
      cors: true
    }
  });

  server.route({
    method: 'GET',
    path: '/beta/stock/{symbol}/chart/{period}',
    handler: async (req) => {
      const url = `${BASE_PATH}/beta/stock/${req.params.symbol}/chart/${req.params.period}?token=${req.query.token}`;
      return server.methods.fetchPriceQuery(url);
    }
  });

  server.method('fetchPriceQuery',
    (url: string) => {
      return new Promise((resolve, reject) => {
        require('request')(url, (error, response, body) => {
          if (response && response.statusCode === 200) {
            resolve(body);
          } else if (error) {
            reject(error);
          } else {
            reject(new HttpErrorResponse(
              {
                error: 'Unknown error has occurred!'
              })
            );
          }
        });
      });
    },
    {
    cache: {
      expiresIn: 10 * ONE_MINUTE,
      generateTimeout: 1500
    }
  });

  await server.start();
  console.log('Server running on %s', server.info.uri);
};

process.on('unhandledRejection', err => {
  console.log(err);
  process.exit(1);
});

init();
